<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Artist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artist', function (Blueprint $table) {
            $table->increments('id');
            $table->string('artist_name');
            $table->text('artist_details')->nullable();
            $table->integer('genre_id')->unsigned();
            $table->foreign('genre_id')->references('id')->on('genre')
                ->onUpdate('no action')->onDelete('no action');
            $table->timestamps();
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('artist');
       
    }
}
