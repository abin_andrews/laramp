<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Album extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('album', function (Blueprint $table) {
            $table->increments('id');
            $table->string('album_name');
            $table->text('album_details')->nullable();
            $table->integer('artist_id')->unsigned();
            $table->foreign('artist_id')->references('id')->on('artist')
                ->onUpdate('no action')->onDelete('no action');
            $table->integer('genre_id')->unsigned();
            $table->foreign('genre_id')->references('id')->on('genre')
                ->onUpdate('no action')->onDelete('no action');
            $table->timestamps();
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('album');
        
    }
}
