<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Song extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('song', function (Blueprint $table) {
            $table->increments('id');
            $table->string('song_name');
            $table->text('song_details')->nullable();
            $table->string('year', 4)->nullable();
            $table->string('language')->nullable();
            $table->string('country')->nullable();
            $table->tinyInteger('isOriginal')->nullable();
            $table->tinyInteger('flag');
            $table->integer('album_id')->unsigned();
            $table->foreign('album_id')->references('id')->on('album')
                ->onUpdate('no action')->onDelete('no action');
            $table->integer('artist_id')->unsigned();
            $table->foreign('artist_id')->references('id')->on('artist')
                ->onUpdate('no action')->onDelete('no action');
            $table->integer('file_id')->unsigned();
            $table->foreign('file_id')->references('id')->on('file')
                ->onUpdate('no action')->onDelete('cascade');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('no action')->onDelete('no action');
            $table->timestamps();
        });

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('song');
        
    }
}
