<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Emotion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emotion', function (Blueprint $table) {
            $table->increments('id');
            $table->string('emotion_name');
        });

        Schema::create('emotion_song', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('emotion_id')->unsigned();
            $table->foreign('emotion_id')->references('id')->on('emotion')
                ->onUpdate('no action')->onDelete('no action');
            $table->integer('song_id')->unsigned();
            $table->foreign('song_id')->references('id')->on('song')
                ->onUpdate('no action')->onDelete('no action');
            $table->timestamps();
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('emotion');
        Schema::drop('emotion_song');
       
    }
}
